<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>knowledgeTime | About</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/edua-icons.css">
<link rel="stylesheet" type="text/css" href="css/animate.min.css">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
<link rel="stylesheet" type="text/css" href="css/cubeportfolio.min.css">
<link rel="stylesheet" type="text/css" href="css/settings.css">
<link rel="stylesheet" type="text/css" href="css/bootsnav.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/loader.css">

<link rel="icon" href="images/favicon.png">


<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--Loader-->
<div class="loader">
  <div class="bouncybox">
      <div class="bouncy"></div>
    </div>
</div>

<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pull-left">
        <span class="info"><a href="#."> Website is under construction! Please contact for more details.</a></span>
        <span class="info"><i class="icon-phone2"></i>+1-514-692-9467</span>
        <span class="info"><i class="icon-mail"></i>contact@knowledgetime.org</span>
        </div>
        <ul class="social_top pull-right">
          <li><a href="https://www.facebook.com/knowledgetime.org/"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/knowledge_time"><i class="icon-twitter4"></i></a></li>
          <li><a href="#."><i class="icon-google"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!--Header-->
<header>
  <nav class="navbar navbar-default navbar-fixed white no-background bootsnav">
    <div class="container">
       <div class="search_btn btn_common"><i class="icon-icons185"></i></div>
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
          <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="index.php"><img src="images/logo-white.png" alt="logo" class="logo logo-display">
        <img src="images/logo.png" class="logo logo-scrolled" alt="">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOut">
          <li>
            <a href="index.php" class="dropdown-toggle" data-toggle="dropdown" >Home</a>
           </li>
          <li>
            <a href="courses.php" class="dropdown-toggle" data-toggle="dropdown" >courses</a>
            </li>
        <!--  <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" >events</a>
            <ul class="dropdown-menu">
              <li><a href="event.php">events</a></li>
              <li><a href="event_detail.php">Events Detail</a></li>
            </ul>
          </li>
-->
        <!--  <li class="dropdown megamenu-fw">-->
            <li>
            <a href="about.php" class="dropdown-toggle" data-toggle="dropdown">About</a>
          </li>
          <!--  <ul class="dropdown-menu megamenu-content" role="menu">
              <li>
                <div class="row">
                  <div class="col-menu col-md-3">
                    <h6 class="title">Pages</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="about.php">About</a></li>
                        <li><a href="testinomial.php">Testinomial</a></li>
                        <li><a href="our_team.php">Our team</a></li>
                  <li><a href="pricing.php">Pricings</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Blog</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="blog.php">Blog</a></li>
                        <li><a href="blog/blog2.php">Blog 02</a></li>
                        <li><a href="blog/blog3.php">Blog 03</a></li>
                        <li><a href="blog_detail.php">Blog Detail</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Shop</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="shop.php">Shop</a></li>
                        <li><a href="shop_detail.php">Shop Detail</a></li>
                        <li><a href="shop_cart.php">Cart</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Others</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="gallery.php">Gallery</a></li>
                        <li><a href="faq.php">Faq</a></li>
                        <li><a href="404.php">404</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>-->
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact.php">Contact</a></li>
          <li><a href="login.php">Login</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>


<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>


<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>About US</h1>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="index.php">Home</a> <span><i class="fa fa-angle-double-right"></i>About Us</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->


<!--ABout US-->
<section id="about" class="padding">
  <div class="container aboutus">
    <div class="row">
      <div class="col-md-7 wow fadeInLeft" data-wow-delay="300ms">
       <h2 class="heading heading_space">Welcome to KnowledgeTime <span class="divider-left"></span></h2>
       <h4 class="bottom25">It’s all going  about making ideas happen. KnowledgeTime is a web-based platform that is designed to offer innovative and easily accessible lectures delivered by accomplished professors and industry experts.   </h4>
       <p class="bottom25">Highly passionate lecturers, professors and industrialists are our assets. This online platform allows students, the unique opportunity to learn the subjects of their interest at their own convenient time and pace. Our goal for the young minds is, to reach their dreams easier and faster. </p>
       <p class="bottom25">We offer all the students an opportunity to enhance their creative thinking, problem solving skills, find their passion and gain the required skills for their dream career</p>
      </div>
      <div class="col-md-5 wow fadeInRight" data-wow-delay="300ms">
        <div class="image">
         <img src="images/about-us.jpg" alt="Edua">
        </div>
      </div>
    </div>
  </div>
</section>
<!--ABout US-->

<!-- Company History -->
<!--  <section id="history" class="padding bg_grey">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInDown">
       <h2 class="heading heading_space">Company History <span class="divider-left"></span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-sm-6 history_wrap bottom25 wow fadeIn" data-wow-delay="300ms">
        <div class="row">
          <div class="col-lg-5 col-md-5 col-sm-12">
            <div class="image"><img src="images/history1.jpg" alt="our history"></div>
          </div>
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <h3><span>1991</span> . Structure was Founded</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua.
            occaecat aute irure dolor in reprehenderit.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 history_wrap bottom25 wow fadeIn" data-wow-delay="400ms">
        <div class="row">
          <div class="col-lg-5 col-md-5 col-sm-12">
            <div class="image"><img src="images/history2.jpg" alt="our history"></div>
          </div>
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <h3><span>1991</span> . Structure was Founded</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua.
            occaecat aute irure dolor in reprehenderit.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 history_wrap bottom25 wow fadeIn" data-wow-delay="500ms">
        <div class="row">
          <div class="col-lg-5 col-md-5 col-sm-12">
            <div class="image"><img src="images/history3.jpg" alt="our history"></div>
          </div>
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <h3><span>1991</span> . Structure was Founded</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua.
            occaecat aute irure dolor in reprehenderit.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 history_wrap bottom25 wow fadeIn" data-wow-delay="600ms">
        <div class="row">
          <div class="col-lg-5 col-md-5 col-sm-12">
            <div class="image"><img src="images/history4.jpg" alt="our history"></div>
          </div>
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <h3><span>1991</span> . Structure was Founded</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua.
            occaecat aute irure dolor in reprehenderit.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>     -->
<!-- Company History -->

<!--Fun Facts-->
<section id="counter" class="parallax padding">
  <div class="container">
    <h2 class="hidden">hidden</h2>
    <div class="row number-counters">
      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="300ms">
        <i class="icon-checkmark3"></i>
        <strong data-to="1235">0</strong>
        <p>Project Completed</p>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="400ms">
        <i class="icon-trophy"></i>
        <strong data-to="78">0</strong>
        <p>Awards Won</p>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="500ms">
        <i class=" icon-icons20"></i>
        <strong data-to="186">0</strong>
        <p>Hours of Work / Month</p>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="600ms">
        <i class="icon-happy"></i>
        <strong data-to="89">0</strong>
        <p>Satisfied Clients</p>
      </div>
    </div>
  </div>
</section>
<!--Fun Facts-->




<!-- University Tour -->
<section id="tours" class="bg_grey padding">
  <div class="container tour_media">
    <div class="row">
      <div class="tour_body wow fadeInLeft" data-wow-delay="300ms">
        <h2 class="heading heading_space">University Tour<span class="divider-left"></span></h2>
        <h4 class="bottom25">It’s all going  about making ideas happen. Nor again is anyone who loves or pursues or desires </h4>
        <p class="bottom25">Proin sagittis feugiat elit finibus pretium. Donec et tortor non purus vulputate tincidunt. Cras congue posuer eros eget egestas.
          Aenean varius ex ut ex laoreet fermentum. odio Proin mattis congue tristique.
        </p>
        <a class="btn_common blue border_radius" href="#">Read more</a>
      </div>
      <div class="tour_feature wow fadeInRight" data-wow-delay="300ms">
        <img src="images/tour.jpg" alt="our Tour">
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div id="projects" class="cbp">
        <div class="cbp-item">
          <img src="images/tour1.jpg" alt="">
          <div class="overlay">
          <div class="centered text-center">
            <a href="images/tour1.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
        </div>
        <div class="cbp-item">
          <img src="images/tour2.jpg" alt="">
          <div class="overlay">
          <div class="centered text-center">
            <a href="images/tour2.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
        </div>
        <div class="cbp-item">
          <img src="images/tour3.jpg" alt="">
          <div class="overlay">
          <div class="centered text-center">
            <a href="images/tour3.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
        </div>
        <div class="cbp-item">
          <img src="images/tour4.jpg" alt="">
          <div class="overlay">
          <div class="centered text-center">
            <a href="images/tour4.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
        </div>
        <div class="cbp-item">
          <img src="images/tour5.jpg" alt="">
          <div class="overlay">
          <div class="centered text-center">
            <a href="images/tour5.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- University Tour -->



<!-- Teachers -->
<section id="teachers" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInDown">
        <h2 class="heading heading_space">Our Teachers<span class="divider-left"></span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="slider_wrapper">
          <div id="director_slider" class="owl-carousel">
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher1.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher2.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher3.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher1.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon blac bottom5k">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher2.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher3.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Teachers -->


<!--FOOTER-->
<footer class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">About Us<span class="divider-left"></span></h3>
        <a href="index.php" class="footer_logo bottom25"><img src="images/logo-white.png" alt="Edua"></a>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <ul class="social_icon top25">
          <li><a href="https://www.facebook.com/knowledgetime.org/" class="facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/knowledge_time" class="twitter"><i class="icon-twitter4"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">Quick Links<span class="divider-left"></span></h3>
        <ul class="links">
                  <li><a href="index.php"><i class="icon-chevron-small-right"></i>Home</a></li>
                  <li><a href="our_team.php"><i class="icon-chevron-small-right"></i>Our Team</a></li>
                  <li><a href="gallery.php"><i class="icon-chevron-small-right"></i>Gallery</a></li>
                <li><a href="certification.php"><i class="icon-chevron-small-right"></i>Certifications</a></li>
                  <li><a href="faq.php"><i class="icon-chevron-small-right"></i>FAQ</a></li>
                  <li><a href="privacy_policy.php"><i class="icon-chevron-small-right"></i>Privacy Policy</a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">Keep in Touch <span class="divider-left"></span></h3>
        <p class=" address"><i class="icon-map-pin"></i>Montreal, Canada</p>
        <p class=" address"><i class="icon-phone"></i>(514)-692-9467</p>
        <p class=" address"><i class="icon-mail"></i><a href="mailto:Edua@info.com">contact@knowledgetime.org</a></p>
        <img src="images/footer-map.png" alt="we are here" class="img-responsive">
      </div>
    </div>
  </div>
</footer>
<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <p>Copyright &copy; 2016 <a href="home.php">KnowledgeTime</a>. all rights reserved.</p>
      </div>
    </div>
  </div>
</div>
<!--FOOTER ends-->

<script src="js/jquery-2.2.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootsnav.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery-countTo.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution.extension.video.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/functions.js"></script>

</body>
</html>
