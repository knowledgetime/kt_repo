<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>KnowledgeTime</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/edua-icons.css">
<link rel="stylesheet" type="text/css" href="css/animate.min.css">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
<link rel="stylesheet" type="text/css" href="css/cubeportfolio.min.css">
<link rel="stylesheet" type="text/css" href="css/settings.css">
<link rel="stylesheet" type="text/css" href="css/bootsnav.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/loader.css">

<link rel="icon" href="images/favicon.png">

<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--Loader-->
<div class="loader">
  <div class="bouncybox">
      <div class="bouncy"></div>
    </div>
</div>

<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pull-left">
        <span class="info"><a href="#."> Website is under construction! Please contact for more details.</a></span>
        <span class="info"><i class="icon-phone2"></i>+1-514-692-9467</span>
        <span class="info"><i class="icon-mail"></i>contact@knowledgetime.org</span>
        </div>
        <ul class="social_top pull-right">
          <li><a href="https://www.facebook.com/knowledgetime.org/"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/knowledge_time"><i class="icon-twitter4"></i></a></li>
          <li><a href="#."><i class="icon-google"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!--Header-->
<header>
  <nav class="navbar navbar-default navbar-fixed white no-background bootsnav">
    <div class="container">
       <div class="search_btn btn_common"><i class="icon-icons185"></i></div>
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
          <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="index.php"><img src="images/logo-white.png" alt="logo" class="logo logo-display">
        <img src="images/logo.png" class="logo logo-scrolled" alt="">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOut">
          <li>
            <a href="index.php" class="dropdown-toggle" data-toggle="dropdown" >Home</a>
           </li>
          <li>
            <a href="courses.php" class="dropdown-toggle" data-toggle="dropdown" >courses</a>
            </li>
        <!--  <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" >events</a>
            <ul class="dropdown-menu">
              <li><a href="event.php">events</a></li>
              <li><a href="event_detail.php">Events Detail</a></li>
            </ul>
          </li>
-->
        <!--  <li class="dropdown megamenu-fw">-->
            <li>
            <a href="about.php" class="dropdown-toggle" data-toggle="dropdown">About</a>
          </li>
          <!--  <ul class="dropdown-menu megamenu-content" role="menu">
              <li>
                <div class="row">
                  <div class="col-menu col-md-3">
                    <h6 class="title">Pages</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="about.php">About</a></li>
                        <li><a href="testinomial.php">Testinomial</a></li>
                        <li><a href="our_team.php">Our team</a></li>
                  <li><a href="pricing.php">Pricings</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Blog</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="blog.php">Blog</a></li>
                        <li><a href="blog/blog2.php">Blog 02</a></li>
                        <li><a href="blog/blog3.php">Blog 03</a></li>
                        <li><a href="blog_detail.php">Blog Detail</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Shop</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="shop.php">Shop</a></li>
                        <li><a href="shop_detail.php">Shop Detail</a></li>
                        <li><a href="shop_cart.php">Cart</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Others</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="gallery.php">Gallery</a></li>
                        <li><a href="faq.php">Faq</a></li>
                        <li><a href="404.php">404</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>-->
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact.php">Contact</a></li>
          <li><a href="login.php">Login</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>


<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>

<!--Slider-->
<section class="rev_slider_wrapper text-center">
<!-- START REVOLUTION SLIDER 5.0 auto mode -->
  <div id="rev_slider" class="rev_slider"  data-version="5.0">
    <ul>
    <!-- SLIDE  -->
      <li data-transition="fade">
        <!-- MAIN IMAGE -->
        <img src="images/banner1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgparallax="10" class="rev-slidebg">
        <!-- LAYER NR. 1 -->
        <div class="tp-caption tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['326','270','270','150']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','on','on']"
        data-transform_idle="o:1;"
        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
        data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
        data-start="800"><h1>Live Online Learning</h1>
        </div>
        <!--<div class="tp-caption tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['380','340','300','350']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','off','off']"
        data-transform_idle="o:1;"
        data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;"
        data-transform_out="opacity:0;s:1000;s:1000;"
      data-start="1500"><p>Learn the cutting edge courses live<br/> by world's best passionate Industry and Academic experts</p>
    </div>-->
        <div class="tp-caption  tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['400','360','320','370']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','on','on']"
        data-transform_idle="o:1;"
        data-transform_in="y:[-200%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
        data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
        data-mask_out="x:0;y:0;s:inherit;e:inherit;"
        data-start="2000">
      <!--  <a href="#." class="border_radius btn_common white_border">our services</a>
      -->  <a href="courses.php" class="border_radius btn_common blue">View Courses</a>
        </div>

      </li>

      <li data-transition="fade">
        <img src="images/banner2.jpg"  alt="" data-bgposition="center center" data-bgfit="cover" data-bgparallax="10" class="rev-slidebg">
        <div class="tp-caption tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['326','270','270','150']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','on','on']"
        data-transform_idle="o:1;"
        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
        data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
        data-start="800"><h1>Take the first step towards the success</h1>
        </div>
      <!--  <div class="tp-caption tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['380','340','300','350']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','off','off']"
        data-transform_idle="o:1;"
        data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;"
        data-transform_out="opacity:0;s:1000;s:1000;"
        data-start="1500"><p>Enjoy knowledge transfer by <br/> passionate experts </p>
      </div>-->
        <div class="tp-caption  tp-resizeme"
        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
        data-y="['400','360','320','370']" data-voffset="['0','0','0','0']"
        data-responsive_offset="on"
        data-visibility="['on','on','on','on']"
        data-transform_idle="o:1;"
        data-transform_in="y:[-200%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
        data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
        data-mask_out="x:0;y:0;s:inherit;e:inherit;"
        data-start="2000">
        <a href="courses.php" class="border_radius btn_common blue">View Courses</a>
      </div>
      </li>
    </ul>
  </div><!-- END REVOLUTION SLIDER -->
</section>


<!--ABout US-->
<section id="about" class="padding">
  <div class="container">
    <div class="row">
    <div class="icon_wrap padding-bottom-half clearfix">
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="300ms">
         <i class="icon-icons9"></i>
         <h4 class="text-capitalize bottom20 margin10">Live online courses</h4>
         <p class="no_bottom">Learn the latest trends and skills live from the best industry and academic experts.</p>
      </div>
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="400ms">
         <i class="icon-icons9"></i>
         <h4 class="text-capitalize bottom20 margin10">Best Industry Leaders</h4>
         <p class="no_bottom">Opportunity to learn the cutting edge technologies from the best industry epxerts.</p>
      </div>
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="500ms">
         <i class="icon-icons20"></i>
         <h4 class="text-capitalize bottom20 margin10">Research and Innovative learning</h4>
         <p class="no_bottom">Understand the future of subjects and master your skill from the renowed researchers across the world.</p>
      </div>
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="600ms">
         <i class="icon-globe"></i>
         <h4 class="text-capitalize bottom20 margin10">Projects by experts</h4>
         <p class="no_bottom">Involve in the practical projects and get guidance by experts</p>
      </div>
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="400ms">
         <i class="icon-layers"></i>
         <h4 class="text-capitalize bottom20 margin10">Cutting edge technologies</h4>
         <p class="no_bottom">Get the knowledge of cutting edge technologies</p>
      </div>
      <div class="col-sm-4 icon_box text-center heading_space wow fadeInUp" data-wow-delay="500ms">
         <i class="icon-laptop"></i>
         <h4 class="text-capitalize bottom20 margin10">Opportunity to discuss with experts</h4>
         <p class="no_bottom">Opportunity to discuss with Industry and academic experts</p>
      </div>
      </div>
    </div>
  </div>
  <div class="container margin_top">
    <div class="row">
      <div class="col-md-7 col-sm-6 priorty wow fadeInLeft" data-wow-delay="300ms">
        <h2 class="heading bottom25">Welcome to KnowledgeTime<span class="divider-left"></span></h2>
        <p class="half_space">KnowledgeTime is a web-based platform that is designed to offer innovative and easily accessible lectures delivered by accomplished professors and industry experts through online courses.</p>
        <div class="row">
          <div class="col-md-6">
            <div class="about-post">
            <a href="#." class="border_radius"><img src="images/hands.png" alt="hands"></a>
            <h4>Best experts</h4>
            <p>Learn from best experts</p>
            </div>
            <div class="about-post">
            <a href="#." class="border_radius"><img src="images/awesome.png" alt="hands"></a>
            <h4>Technology enabled learning</h4>
            <p>Learning is fun with new technoogies</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="about-post">
            <a href="#." class="border_radius"><img src="images/maintenance.png" alt="hands"></a>
            <h4>Live online courses</h4>
            <p>Its fun to learn from best experts through live courses</p>
            </div>
            <div class="about-post">
            <a href="#." class="border_radius"><img src="images/home.png" alt="hands"></a>
            <h4>KnowledgeTime team</h4>
            <p>Passionate team who are dedicated to give you the best</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-5 col-sm-6 wow fadeInRight" data-wow-delay="300ms">
         <img src="images/about.jpg" alt="our priorties" class="img-responsive" style="width:100%;">
      </div>
    </div>
  </div>
</section>
<!--ABout US-->


<!-- Courses -->
<section id="courses" class="padding parallax">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="heading heading_space wow fadeInDown">Popular Courses<span class="divider-left"></span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="slider_wrapper">
          <div id="course_slider" class="owl-carousel">
            <div class="item">
              <div class="image bottom20">
                <img src="images/courses/robotics.jpg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="course_detail.php">Robotics</a></h3>
              <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <a href="course_detail.php" class="btn_common blue border_radius">Read More</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="images/courses/control_to_space.jpeg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="course_detail.php">Control Engineering to Space technology</a></h3>
              <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <a href="course_detail.php" class="btn_common blue border_radius">Read More</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="images/courses/electronics_product_design.jpg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="course_detail.php">Electronic product design</a></h3>
              <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <a href="course_detail.php" class="btn_common blue border_radius">Read More</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="images/courses/python.jpeg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="course_detail.php">Python programming</a></h3>
              <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <a href="course_detail.php" class="btn_common blue border_radius">Read More</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="images/courses/data_analysis.jpeg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="course_detail.php">Data Analysis</a></h3>
              <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <a href="course_detail.php" class="btn_common blue border_radius">Read More</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="images/courses/internet_of_things.jpg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="course_detail.php">Internet of Things</a></h3>
              <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <a href="course_detail.php" class="btn_common blue border_radius">Read More</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="images/courses/machine_learning.jpg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="course_detail.php">Machine Learning</a></h3>
              <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <a href="course_detail.php" class="btn_common blue border_radius">Read More</a>
            </div>
            <div class="item">
              <div class="image bottom20">
                <img src="images/courses/artificial_intelligence.jpeg" alt="Courses" class="img-responsive border_radius">
              </div>
              <h3 class="bottom15"><a href="course_detail.php">Artificial Intelligence</a></h3>
              <p class="bottom15">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <a href="course_detail.php" class="btn_common blue border_radius">Read More</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Courses -->

<!--Fun Facts-->
<section id="facts" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center wow fadeInDown">
       <h2 class="heading">KnowledgeTime<span class="divider-center"></span></h2>
       <p class="heading_space margin10">Quality education for all</p>
      </div>
    </div>
    <div class="row number-counters">
      <div class="col-md-2 col-sm-4">
        <div class="counters-item">
        <i class="icon-checkmark3"></i>
        <strong data-to="1235">0</strong>
        <!-- Set Your Number here. i,e. data-to="56" -->
        <p>Project Completed</p>
        </div>
        <div class="counters-item last">
        <i class="icon-trophy"></i>
        <strong data-to="78">0</strong>
        <p>Awards Won</p>
        </div>
      </div>
      <div class="col-md-7 col-sm-4">
        <div class="fact-image">
        <img src="images/fun-facts.png" alt=" some facts" class="img-responsive">
        </div>
      </div>
      <div class="col-md-3 col-sm-4">
       <div class="counters-item">
        <i class=" icon-icons20"></i>
        <strong data-to="186">0</strong>
        <p>Hours of Work / Month</p>
        </div>
        <div class="counters-item last">
        <i class="icon-happy"></i>
        <strong data-to="89">0</strong>
        <p>Satisfied Clients</p>
        </div>
      </div>
    </div>
  </div>
</section>

<!--Customers Review-->
<section id="reviews" class="padding bg_light">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center wow fadeInDown">
      <h2 class="heading heading_space">What People say <span class="divider-center"></span></h2>
      <div id="review_slider" class="owl-carousel text-center">
        <div class="item">
          <h4>Shubhashish Mehra</h4>
          <p>MBA</p>
          <img src="images/customer1.jpg" class="client_pic border_radius" alt="costomer">
          <p>KnowledgeTime is a great innovative online educational platform. In today's fast pacing and competitive environment, this next generation way of learning was really missing but thankfully we have one now. The best part I like about knowledge time is its ability to combine the academics with industry and research to have the better understanding of any concept and get the best out of our time that too as it says at our own time, pace and space.</p>
        </div>
        <div class="item">
           <h4>apple</h4>
          <p>Ditector Shangha</p>
          <img src="images/customer1.png" class="client_pic border_radius" alt="costomer">
          <p>I've been happy with the services provided by Edua LLC. Scooter Libby has been wonderful! He has returned my calls quickly, and he answered all my questions. This is required when, for example, the final text is not yet available. We are here to help you from the initial phase to the final Edua phase.</p>
        </div>
        <div class="item">
           <h4>John Smith</h4>
          <p>Ditector Shangha</p>
          <img src="images/customer1.png" class="client_pic border_radius" alt="costomer">
          <p>I've been happy with the services provided by Edua LLC. Scooter Libby has been wonderful! He has returned my calls quickly, and he answered all my questions. This is required when, for example, the final text is not yet available. We are here to help you from the initial phase to the final Edua phase.</p>
        </div>
       </div>
      </div>
    </div>
  </div>
</section>




      <!--Pricings-->
<!--   <section class="padding" id="pricing">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center wow fadeInDown">
        <h2 class="heading">Pricing Tables <span class="divider-center"></span></h2>
        <p class="heading_space margin10">Choose the suitable plan for you</p>
      </div>
      <div class="col-md-12">
        <div class="pricing">
          <div class="pricing_item wow fadeInUp" data-wow-delay="300ms">
            <h3>Basic</h3>
            <div class="pricing_price"><span class="pricing_currency">$</span>9.90</div>
            <p class="pricing_sentence">Perfect for users</p>
            <ul class="pricing_list">
              <li class="pricing_feature">Support forum</li>
              <li class="pricing_feature">Free hosting</li>
              <li class="pricing_feature">40MB of storage space</li>
              <li>Social media integration</li>
              <li>1GB of storage space</li>
            </ul>
            <a class="btn_common text-center" href="#.">Choose plan</a>
          </div>
          <div class="pricing_item active wow fadeInUp" data-wow-delay="400ms">
            <h3>Popular</h3>
            <div class="pricing_price"><span class="pricing_currency">$</span>29,90</div>
            <p class="pricing_sentence">Suitable for small businesses with up to 5 employees</p>
            <ul class="pricing_list">
              <li class="pricing_feature">Unlimited calls</li>
              <li class="pricing_feature">Free hosting</li>
              <li class="pricing_feature">10 hours of support</li>
              <li class="pricing_feature">Social media integration</li>
              <li class="pricing_feature">1GB of storage space</li>
            </ul>
            <a class="btn_common text-center" href="#.">Choose plan</a>
          </div>
          <div class="pricing_item dark_gray wow fadeInUp" data-wow-delay="500ms">
            <h3>Premier</h3>
            <div class="pricing_price"><span class="pricing_currency">$</span>59,90</div>
            <p class="pricing_sentence">Great for large businesses with more than 5 employees</p>
            <ul class="pricing_list">
              <li class="pricing_feature">Unlimited calls</li>
              <li class="pricing_feature">Free hosting</li>
              <li class="pricing_feature">Unlimited hours of support</li>
              <li class="pricing_feature">Social media integration</li>
              <li class="pricing_feature">Unlimited storage space</li>
            </ul>
            <a class="btn_common text-center" href="#.">Choose plan</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>    -->
<!--Pricings-->


<!--Paralax -->
<section id="parallax" class="parallax">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center wow bounceIn">
       <h2>We Believe Albert Einstein's saying Wisdom is not a product of schooling but of the lifelong attempt to acquire it</h2>
       <h1 class="margin10">Since 2016</h1>
       <a href="our_team.php" class="border_radius btn_common white_border margin10">KnowledgeTime</a>
      </div>
    </div>
  </div>
</section>
<!--Paralax -->


<!-- News-->
<section id="news" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInDown">
       <h2 class="heading heading_space">Latest updates<span class="divider-left"></span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="slider_wrapper">
          <div id="news_slider" class="owl-carousel">
            <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="images/news1.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="blog_detail.php">4 Springtime Color Schemes to Try at Home</a></h4>
                  <ul class="commment">
                    <li><a href="#."><i class="icon-icons20"></i>June 6, 2016</a></li>
                    <li><a href="#."><i class="icon-comment"></i> 02</a></li>
                  </ul>
                  <p>We offer the most complete house Services in the country...</p>
                  <a href="blog_detail.php" class="readmore">Read More</a>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="images/news2.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="blog_detail.php"> Springtime Color Schemes to Try at Home</a></h4>
                  <ul class="commment">
                    <li><a href="#."><i class="icon-icons20"></i>June 6, 2016</a></li>
                    <li><a href="#."><i class="icon-comment"></i> 02</a></li>
                  </ul>
                  <p>We offer the most complete house Services in the country...</p>
                  <a href="blog_detail.php" class="readmore">Read More</a>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="images/news3.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="blog_detail.php">4 Springtime Color Schemes to Try at Home</a></h4>
                  <ul class="commment">
                    <li><a href="#."><i class="icon-icons20"></i>June 6, 2016</a></li>
                    <li><a href="#."><i class="icon-comment"></i> 02</a></li>
                  </ul>
                  <p>We offer the most complete house Services in the country...</p>
                  <a href="blog_detail.php" class="readmore">Read More</a>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="images/news1.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="blog_detail.php">4 Springtime Color Schemes to Try at Home</a></h4>
                  <ul class="commment">
                    <li><a href="#."><i class="icon-icons20"></i>June 6, 2016</a></li>
                    <li><a href="#."><i class="icon-comment"></i> 02</a></li>
                  </ul>
                  <p>We offer the most complete house Services in the country...</p>
                  <a href="blog_detail.php" class="readmore">Read More</a>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="images/news2.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="blog_detail.php">4 Springtime Color Schemes to Try at Home</a></h4>
                  <ul class="commment">
                    <li><a href="#."><i class="icon-icons20"></i>June 6, 2016</a></li>
                    <li><a href="#."><i class="icon-comment"></i> 02</a></li>
                  </ul>
                  <p>We offer the most complete house Services in the country...</p>
                  <a href="blog_detail.php" class="readmore">Read More</a>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="content_wrap">
                <div class="image">
                  <img src="images/news3.jpg" alt="Edua" class="img-responsive border_radius">
                </div>
                <div class="news_box border_radius">
                  <h4><a href="blog_detail.php">4 Springtime Color Schemes to Try at Home</a></h4>
                  <ul class="commment">
                    <li><a href="#."><i class="icon-icons20"></i>June 6, 2016</a></li>
                    <li><a href="#."><i class="icon-comment"></i> 02</a></li>
                  </ul>
                  <p>We offer the most complete house Services in the country...</p>
                  <a href="blog_detail.php" class="readmore">Read More</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



<!--FOOTER-->
<footer class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">About Us<span class="divider-left"></span></h3>
        <a href="index.php" class="footer_logo bottom25"><img src="images/logo-white.png" alt="Edua"></a>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <ul class="social_icon top25">
          <li><a href="https://www.facebook.com/knowledgetime.org/" class="facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/knowledge_time" class="twitter"><i class="icon-twitter4"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">Quick Links<span class="divider-left"></span></h3>
        <ul class="links">
                  <li><a href="index.php"><i class="icon-chevron-small-right"></i>Home</a></li>
                  <li><a href="our_team.php"><i class="icon-chevron-small-right"></i>Our Team</a></li>
                  <li><a href="gallery.php"><i class="icon-chevron-small-right"></i>Gallery</a></li>
                <li><a href="certification.php"><i class="icon-chevron-small-right"></i>Certifications</a></li>
                  <li><a href="faq.php"><i class="icon-chevron-small-right"></i>FAQ</a></li>
                  <li><a href="privacy_policy.php"><i class="icon-chevron-small-right"></i>Privacy Policy</a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">Keep in Touch <span class="divider-left"></span></h3>
        <p class=" address"><i class="icon-map-pin"></i>Montreal, Canada</p>
        <p class=" address"><i class="icon-phone"></i>(514)-692-9467</p>
        <p class=" address"><i class="icon-mail"></i><a href="mailto:Edua@info.com">contact@knowledgetime.org</a></p>
        <img src="images/footer-map.png" alt="we are here" class="img-responsive">
      </div>
    </div>
  </div>
</footer>
<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <p>Copyright &copy; 2016 <a href="home.php">KnowledgeTime</a>. all rights reserved.</p>
      </div>
    </div>
  </div>
</div>
<!--FOOTER ends-->

<script src="js/jquery-2.2.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootsnav.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery-countTo.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution.extension.video.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/functions.js"></script>

</body>
</html>
