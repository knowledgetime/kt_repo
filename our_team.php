<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>KnowledgeTime | Our team</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/edua-icons.css">
<link rel="stylesheet" type="text/css" href="css/animate.min.css">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
<link rel="stylesheet" type="text/css" href="css/cubeportfolio.min.css">
<link rel="stylesheet" type="text/css" href="css/settings.css">
<link rel="stylesheet" type="text/css" href="css/bootsnav.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/loader.css">
<link rel="icon" href="images/favicon.png">


<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--Loader-->
<div class="loader">
  <div class="bouncybox">
      <div class="bouncy"></div>
    </div>
</div>

<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pull-left">
        <span class="info"><a href="#."> Website is under construction! Please contact for more details.</a></span>
        <span class="info"><i class="icon-phone2"></i>+1-514-692-9467</span>
        <span class="info"><i class="icon-mail"></i>contact@knowledgetime.org</span>
        </div>
        <ul class="social_top pull-right">
          <li><a href="https://www.facebook.com/knowledgetime.org/"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/knowledge_time"><i class="icon-twitter4"></i></a></li>
          <li><a href="#."><i class="icon-google"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!--Header-->
<header>
  <nav class="navbar navbar-default navbar-fixed white no-background bootsnav">
    <div class="container">
       <div class="search_btn btn_common"><i class="icon-icons185"></i></div>
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
          <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="index.php"><img src="images/logo-white.png" alt="logo" class="logo logo-display">
        <img src="images/logo.png" class="logo logo-scrolled" alt="">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOut">
          <li>
            <a href="index.php" class="dropdown-toggle" data-toggle="dropdown" >Home</a>
           </li>
          <li>
            <a href="courses.php" class="dropdown-toggle" data-toggle="dropdown" >courses</a>
            </li>
        <!--  <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" >events</a>
            <ul class="dropdown-menu">
              <li><a href="event.php">events</a></li>
              <li><a href="event_detail.php">Events Detail</a></li>
            </ul>
          </li>
-->
        <!--  <li class="dropdown megamenu-fw">-->
            <li>
            <a href="about.php" class="dropdown-toggle" data-toggle="dropdown">About</a>
          </li>
          <!--  <ul class="dropdown-menu megamenu-content" role="menu">
              <li>
                <div class="row">
                  <div class="col-menu col-md-3">
                    <h6 class="title">Pages</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="about.php">About</a></li>
                        <li><a href="testinomial.php">Testinomial</a></li>
                        <li><a href="our_team.php">Our team</a></li>
                  <li><a href="pricing.php">Pricings</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Blog</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="blog.php">Blog</a></li>
                        <li><a href="blog/blog2.php">Blog 02</a></li>
                        <li><a href="blog/blog3.php">Blog 03</a></li>
                        <li><a href="blog_detail.php">Blog Detail</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Shop</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="shop.php">Shop</a></li>
                        <li><a href="shop_detail.php">Shop Detail</a></li>
                        <li><a href="shop_cart.php">Cart</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Others</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="gallery.php">Gallery</a></li>
                        <li><a href="faq.php">Faq</a></li>
                        <li><a href="404.php">404</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>-->
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact.php">Contact</a></li>
          <li><a href="login.php">Login</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>


<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>

<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Team</h1>
        <p>Meet the KnowledgeTime team</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="index.php">Home</a> <span><i class="fa fa-angle-double-right"></i>Our team</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->



<!-- Team -->
<section id="teachers" class="padding-bottom">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="300ms">
          <div class="image bottom25">
            <img src="images/team/kiran.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Founder and CTO</span>
          </div>
          <h3>Kiran Kuruvinahsetti</h3>
          <p class="bottom20 margin10">Responsible for executions and technology designs</p>
      <!--    <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="400ms">
          <div class="image bottom25">
            <img src="images/team/nehal.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Co-Founder and President</span>
          </div>
          <h3>Nehal Bhagat</h3>
          <p class="bottom20 margin10">Responsible for software developments</p>
        <!--  <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="500ms">
          <div class="image bottom25">
            <img src="images/team/vinay.jpg" alt="Teachers" class=" border_radius">
            <span class="post">CEO</span>
          </div>
          <h3>Vinay Paramanand</h3>
          <p class="bottom20 margin10"> Responsible for smooth executions in the company</p>
        <!--  <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul> -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="600ms">
          <div class="image bottom25">
            <img src="images/team/naveen.jpg" alt="Teachers" class=" border_radius">
            <span class="post">CIO</span>
          </div>
          <h3>Naveen Shirur</h3>
          <p class="bottom20 margin10">Responsible for Innovations in the company</p>
      <!--    <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="700ms">
          <div class="image bottom25">
            <img src="images/team/pankaj.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Lead course content designer</span>
          </div>
          <h3>Pankaj Shivhare</h3>
          <p class="bottom20 margin10">Responsible for course content design</p>
    <!--      <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="800ms">
          <div class="image bottom25">
            <img src="images/team/amandeep.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Co-Founder and Software Developer</span>
          </div>
          <h3>Amandeep Kaur</h3>
          <p class="bottom20 margin10">Responsible for the software development</p>
          <ul class="social_icon black bottom5">
      <!--      <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="900ms">
          <div class="image bottom25">
            <img src="images/team/lakshman.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Information security Analyst</span>
          </div>
          <h3>Lakshman Pawar</h3>
          <p class="bottom20 margin10">Responsible for maintance of data security</p>
      <!--    <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
          <div class="image bottom25">
            <img src="images/team/natalia.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Lead scientific learning officer</span>
          </div>
          <h3>Natalia Kubishyn</h3>
          <p class="bottom20 margin10">Responsible for Scientific ways of learning</p>
      <!--    <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
          <div class="image bottom25">
            <img src="images/team/gaurav.jpg" alt="Teachers" class=" border_radius">
            <span class="post">Information Security Analyst</span>
          </div>
          <h3>Gaurav Gowda</h3>
          <p class="bottom20 margin10">Responsible for Information security and privacy of the data</p>
      <!--    <ul class="social_icon black bottom5">
            <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
            <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
          </ul>  -->
        </div>
      </div>
        <div class="col-sm-6 col-md-3">
          <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
            <div class="image bottom25">
              <img src="images/team/pallavi.png" alt="Teachers" class=" border_radius">
              <span class="post">Creative Writer</span>
            </div>
            <h3>Pallavi Rajpurohit</h3>
            <p class="bottom20 margin10">Responsible for blogs of the company</p>
      <!--      <ul class="social_icon black bottom5">
              <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
              <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
            </ul>  -->
          </div>
        </div>
          <div class="col-sm-6 col-md-3">
            <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
              <div class="image bottom25">
                <img src="images/team/nandan.jpg" alt="Teachers" class=" border_radius">
                <span class="post">Front end designer</span>
              </div>
              <h3>Nandan Mrn</h3>
              <p class="bottom20 margin10">Responsible for the front end software development </p>
          <!--    <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul> -->
            </div>
          </div>
            <div class="col-sm-6 col-md-3">
              <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
                <div class="image bottom25">
                  <img src="images/team/nitin.jpg" alt="Teachers" class=" border_radius">
                  <span class="post">Finance</span>
                </div>
                <h3>Nitin Hargude</h3>
                <p class="bottom20 margin10">Responsible for finance and planning and executions </p>
          <!--      <ul class="social_icon black bottom5">
                  <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                  <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
                </ul>  -->
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-sm-6 col-md-3">
                <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
                  <div class="image bottom25">
                    <img src="images/team/kaveri.jpg" alt="Teachers" class=" border_radius">
                    <span class="post">Finance</span>
                  </div>
                  <h3>Kaveri Gawade</h3>
                  <p class="bottom20 margin10">Responsible for the finance planning and executions</p>
              <!--    <ul class="social_icon black bottom5">
                    <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                    <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
                  </ul>  -->
                </div>
              </div>
                <div class="col-sm-6 col-md-3">
                  <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
                    <div class="image bottom25">
                      <img src="images/team/neraj.jpg" alt="Teachers" class=" border_radius">
                      <span class="post">Software Developer</span>
                    </div>
                    <h3>Neraj Shivhare</h3>
                    <p class="bottom20 margin10">Responsible for the back end software development</p>
                <!--    <ul class="social_icon black bottom5">
                      <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                      <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
                    </ul> -->
                  </div>
                </div>
                  <div class="col-sm-6 col-md-3">
                    <div class="teacher margin_top wow fadeIn" data-wow-delay="1000ms">
                      <div class="image bottom25">
                        <img src="images/team/mrunalini.jpg" alt="Teachers" class=" border_radius">
                        <span class="post">Front end designer</span>
                      </div>
                      <h3>Mrunalini Lohar</h3>
                      <p class="bottom20 margin10">Responsible for the front end software development</p>
                  <!--    <ul class="social_icon black bottom5">
                        <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                        <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
                      </ul>  -->
                    </div>
                  </div>
                </div>
  </div>
</section>
<!-- Teachers -->

<!--FOOTER-->
<footer class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">About Us<span class="divider-left"></span></h3>
        <a href="index.php" class="footer_logo bottom25"><img src="images/logo-white.png" alt="Edua"></a>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <ul class="social_icon top25">
          <li><a href="https://www.facebook.com/knowledgetime.org/" class="facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/knowledge_time" class="twitter"><i class="icon-twitter4"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">Quick Links<span class="divider-left"></span></h3>
        <ul class="links">
                  <li><a href="index.php"><i class="icon-chevron-small-right"></i>Home</a></li>
                  <li><a href="our_team.php"><i class="icon-chevron-small-right"></i>Our Team</a></li>
                  <li><a href="gallery.php"><i class="icon-chevron-small-right"></i>Gallery</a></li>
                <li><a href="certification.php"><i class="icon-chevron-small-right"></i>Certifications</a></li>
                  <li><a href="faq.php"><i class="icon-chevron-small-right"></i>FAQ</a></li>
                  <li><a href="privacy_policy.php"><i class="icon-chevron-small-right"></i>Privacy Policy</a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">Keep in Touch <span class="divider-left"></span></h3>
        <p class=" address"><i class="icon-map-pin"></i>Montreal, Canada</p>
        <p class=" address"><i class="icon-phone"></i>(514)-692-9467</p>
        <p class=" address"><i class="icon-mail"></i><a href="mailto:Edua@info.com">contact@knowledgetime.org</a></p>
        <img src="images/footer-map.png" alt="we are here" class="img-responsive">
      </div>
    </div>
  </div>
</footer>
<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <p>Copyright &copy; 2016 <a href="home.php">KnowledgeTime</a>. all rights reserved.</p>
      </div>
    </div>
  </div>
</div>
<!--FOOTER ends-->

<script src="js/jquery-2.2.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootsnav.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery-countTo.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution.extension.video.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/functions.js"></script>

</body>
</html>
