<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>KnowledgeTime | Blog</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/edua-icons.css">
<link rel="stylesheet" type="text/css" href="css/animate.min.css">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
<link rel="stylesheet" type="text/css" href="css/cubeportfolio.min.css">
<link rel="stylesheet" type="text/css" href="css/settings.css">
<link rel="stylesheet" type="text/css" href="css/bootsnav.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/loader.css">
<link rel="icon" href="images/favicon.png">

<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--Loader-->
<div class="loader">
  <div class="bouncybox">
      <div class="bouncy"></div>
    </div>
</div>

<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pull-left">
        <span class="info"><a href="#."> Website is under construction! Please contact for more details.</a></span>
        <span class="info"><i class="icon-phone2"></i>+1-514-692-9467</span>
        <span class="info"><i class="icon-mail"></i>contact@knowledgetime.org</span>
        </div>
        <ul class="social_top pull-right">
          <li><a href="https://www.facebook.com/knowledgetime.org/"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/knowledge_time"><i class="icon-twitter4"></i></a></li>
          <li><a href="#."><i class="icon-google"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!--Header-->
<header>
  <nav class="navbar navbar-default navbar-fixed white no-background bootsnav">
    <div class="container">
       <div class="search_btn btn_common"><i class="icon-icons185"></i></div>
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
          <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="index.php"><img src="images/logo-white.png" alt="logo" class="logo logo-display">
        <img src="images/logo.png" class="logo logo-scrolled" alt="">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOut">
          <li>
            <a href="index.php" class="dropdown-toggle" data-toggle="dropdown" >Home</a>
           </li>
          <li>
            <a href="courses.php" class="dropdown-toggle" data-toggle="dropdown" >courses</a>
            </li>
        <!--  <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" >events</a>
            <ul class="dropdown-menu">
              <li><a href="event.php">events</a></li>
              <li><a href="event_detail.php">Events Detail</a></li>
            </ul>
          </li>
-->
        <!--  <li class="dropdown megamenu-fw">-->
            <li>
            <a href="about.php" class="dropdown-toggle" data-toggle="dropdown">About</a>
          </li>
          <!--  <ul class="dropdown-menu megamenu-content" role="menu">
              <li>
                <div class="row">
                  <div class="col-menu col-md-3">
                    <h6 class="title">Pages</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="about.php">About</a></li>
                        <li><a href="testinomial.php">Testinomial</a></li>
                        <li><a href="our_team.php">Our team</a></li>
                  <li><a href="pricing.php">Pricings</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Blog</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="blog.php">Blog</a></li>
                        <li><a href="blog/blog2.php">Blog 02</a></li>
                        <li><a href="blog/blog3.php">Blog 03</a></li>
                        <li><a href="blog_detail.php">Blog Detail</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Shop</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="shop.php">Shop</a></li>
                        <li><a href="shop_detail.php">Shop Detail</a></li>
                        <li><a href="shop_cart.php">Cart</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Others</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="gallery.php">Gallery</a></li>
                        <li><a href="faq.php">Faq</a></li>
                        <li><a href="404.php">404</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>-->
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact.php">Contact</a></li>
          <li><a href="login.php">Login</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>


<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>

<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Blog</h1>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="index.php">Home</a> <span><i class="fa fa-angle-double-right"></i>Blog</span>
      </div>
      </div>
    </div>
  </div>
</section>



<!--BLOG SECTION-->
<section id="blog" class="padding">
  <div class="container">
    <h2 class="hidden">Blog</h2>
    <div class="row">
      <div class="col-md-9">
        <article class="blog_item heading_space wow fadeInLeft" data-wow-delay="300ms">
          <div class="row">
            <div class="col-md-5 col-sm-5 heading_space">
              <div class="image"><img src="images/blog/blog1.jpg" alt="blog" class="border_radius"></div>
            </div>
            <div class="col-md-7 col-sm-7 heading_space">
              <h3>Education- An unceasing trail of changes </h3>
              <ul class="comment margin10">
                <li><a href="#.">Feb 20, 2017</a></li>
                <li><a href="#."><i class="icon-comment"></i> 5</a></li>
              </ul>
              <p class="margin10">Google defines education as ‘The process of receiving or giving systematic instruction, especially at a school or university’. The second definition says - ‘An enlightening experience’. Put together rightfully, education is indeed an enlightening experience we go through by receiving systematic instructions not just at schools or universities, but in the later parts as well.
              </p>
              <p class="margin10">Education forms the base of the society, standing as the core pillar to every other industry, be it communications, textiles, chemicals or trade. </p>
              <a class=" btn_common btn_border margin10 border_radius" href="blog_detail.php">Read More</a>
            </div>
          </div>
        </article>
        <article class="blog_item heading_space wow fadeInLeft" data-wow-delay="400ms">
          <div class="row">
            <div class="col-md-5 col-sm-5 heading_space">
              <div class="image"><img src="images/blog/blog2.jpg" alt="blog" class="border_radius"></div>
            </div>
            <div class="col-md-7 col-sm-7 heading_space">
              <h3>Whats on your mind</h3>
              <ul class="comment margin10">
                <li><a href="#.">Feb 22 2017</a></li>
                <li><a href="#."><i class="icon-comment"></i> 5</a></li>
              </ul>
              <p class="margin10">It was sometime more than a decade back, when one early morning my dad asked me get him the newspaper. Just as I opened it before giving it to him, I could see some familiar continuous words I tend to have had noticed before. When I read through them, I realized that, it was my first poem published in the newspaper. On my! What a titillating moment it was. Seeing my name out there, in one of the leading newspapers, thinking of thousands of people across geography reading it!
              </p>
              <p class="margin10"></p>When I read through them, I realized that, it was my first poem published in the newspaper. On my! What a titillating moment it was. Seeing my name out there, in one of the leading newspapers, thinking of thousands of people across geography reading it!
              <a class=" btn_common btn_border margin10 border_radius" href="blog_detail.html">Read More</a>
            </div>
          </div>
        </article>
  <!--      <article class="blog_item heading_space wow fadeInLeft" data-wow-delay="500ms">
          <div class="row">
            <div class="col-md-5 col-sm-5 heading_space">
              <div class="image"><img src="images/blog3.jpg" alt="blog" class="border_radius"></div>
            </div>
            <div class="col-md-7 col-sm-7 heading_space">
              <h3>11 Times Old Furniture Gained New Life Gained New Life</h3>
              <ul class="comment margin10">
                <li><a href="#.">May 10, 2016</a></li>
                <li><a href="#."><i class="icon-comment"></i> 5</a></li>
              </ul>
              <p class="margin10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab debitis ea est illum ipsa itaque libero optio quasi voluptas! Consequuntur
                deserunt expedita.
              </p>
              <p class="margin10">fugiat hic illum porro quidem quis recusandae vero? Aliquid assumenda cum delectus eaque eligendi, enim eum excepturi fugit illum impedit in
                iste laudantium modi natus […]
              </p>
              <a class=" btn_common btn_border margin10 border_radius" href="blog_detail.php">Read More</a>
            </div>
          </div>
        </article>   -->
        <div class="pager_nav wow fadeIn" data-wow-delay="600ms">
          <ul class="pagination">
            <li class="hidden">
              <a href="#." aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-3">
        <aside class="sidebar bg_grey border-radius wow fadeIn" data-wow-delay="300ms">
          <div class="widget heading_space">
            <form class="widget_search border-radius">
              <div class="input-group">
                <input type="search" class="form-control" placeholder="Search Here" required>
                <i class="input-group-addon icon-icons185"></i>
              </div>
            </form>
          </div>
          <div class="widget heading_space">
            <h3 class="bottom20">Featured Courses</h3>
            <div class="media">
              <a class="media-left" href="#."><img src="images/post1.png" alt="post"></a>
              <div class="media-body">
                <h5 class="bottom5">Artificial Intelligence</h5>
                <a href="#." class="btn-primary border_radius bottom5">free</a>
                <form class="star_rating">
                  <div class="stars">
                    <input type="radio" name="star" class="star-1" id="star-01" />
                    <label class="star-1" for="star-01">1</label>
                    <input type="radio" name="star" class="star-2" id="star-02" />
                    <label class="star-2" for="star-02">2</label>
                    <input type="radio" name="star" class="star-3" id="star-03" />
                    <label class="star-3" for="star-03">3</label>
                    <input type="radio" name="star" class="star-4" id="star-04"  />
                    <label class="star-4" for="star-04">4</label>
                    <input type="radio" name="star" class="star-5"  id="star-05" checked  />
                    <label class="star-5" for="star-05">5</label>
                    <span></span>
                  </div>
                </form>
                <span class="name">Michael Windzor</span>
              </div>
            </div>
            <div class="media">
              <a class="media-left" href="#."><img src="images/post1.png" alt="post"></a>
              <div class="media-body">
                <h5 class="bottom5">Artificial Intelligence</h5>
                <a href="#." class="btn-primary border_radius bottom5">free</a>
                <form class="star_rating">
                  <div class="stars">
                    <input type="radio" name="star" class="star-1" id="star-1" />
                    <label class="star-1" for="star-1">1</label>
                    <input type="radio" name="star" class="star-2" id="star-2" />
                    <label class="star-2" for="star-2">2</label>
                    <input type="radio" name="star" class="star-3" id="star-3" />
                    <label class="star-3" for="star-3">3</label>
                    <input type="radio" name="star" class="star-4" id="star-4"  />
                    <label class="star-4" for="star-4">4</label>
                    <input type="radio" name="star" class="star-5"  id="star-5" checked  />
                    <label class="star-5" for="star-5">5</label>
                    <span></span>
                  </div>
                </form>
                <span class="name">Michael Windzor</span>
              </div>
            </div>
            <div class="media">
              <a class="media-left" href="#."><img src="images/post1.png" alt="post"></a>
              <div class="media-body">
                <h5 class="bottom5">Basic Time Management Course</h5>
                <a href="#." class="btn-primary border_radius bottom5">free</a>
                <form class="star_rating">
                  <div class="stars">
                    <input type="radio" name="star" class="star-1" id="star-11" />
                    <label class="star-1" for="star-11">1</label>
                    <input type="radio" name="star" class="star-2" id="star-12" />
                    <label class="star-2" for="star-12">2</label>
                    <input type="radio" name="star" class="star-3" id="star-13" />
                    <label class="star-3" for="star-13">3</label>
                    <input type="radio" name="star" class="star-4" id="star-14"  />
                    <label class="star-4" for="star-14">4</label>
                    <input type="radio" name="star" class="star-5"  id="star-15" checked  />
                    <label class="star-5" for="star-15">5</label>
                    <span></span>
                  </div>
                </form>
                <span class="name">Michael Windzor</span>
              </div>
            </div>
          </div>
          <div class="widget heading_space">
            <h3 class="bottom20">Top Tags</h3>
            <ul class="tags">
              <li><a href="#.">Books</a></li>
              <li><a href="#.">Midterm test </a></li>
              <li><a href="#.">Presentation</a></li>
              <li><a href="#.">Courses</a></li>
              <li><a href="#.">Certifications</a></li>
              <li><a href="#.">Teachers</a></li>
              <li><a href="#.">Student Life</a></li>
              <li><a href="#.">Study</a></li>
              <li><a href="#.">Midterm test </a></li>
              <li><a href="#.">Presentation</a></li>
              <li><a href="#.">Courses</a></li>
            </ul>
          </div>
        </aside>
      </div>
    </div>
  </div>
</section>
<!--BLOG SECTION-->



<!--FOOTER-->
<footer class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">About Us<span class="divider-left"></span></h3>
        <a href="index.php" class="footer_logo bottom25"><img src="images/logo-white.png" alt="Edua"></a>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <ul class="social_icon top25">
          <li><a href="https://www.facebook.com/knowledgetime.org/" class="facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/knowledge_time" class="twitter"><i class="icon-twitter4"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">Quick Links<span class="divider-left"></span></h3>
        <ul class="links">
                  <li><a href="index.php"><i class="icon-chevron-small-right"></i>Home</a></li>
                  <li><a href="our_team.php"><i class="icon-chevron-small-right"></i>Our Team</a></li>
                  <li><a href="gallery.php"><i class="icon-chevron-small-right"></i>Gallery</a></li>
                <li><a href="certification.php"><i class="icon-chevron-small-right"></i>Certifications</a></li>
                  <li><a href="faq.php"><i class="icon-chevron-small-right"></i>FAQ</a></li>
                  <li><a href="privacy_policy.php"><i class="icon-chevron-small-right"></i>Privacy Policy</a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <h3 class="heading bottom25">Keep in Touch <span class="divider-left"></span></h3>
        <p class=" address"><i class="icon-map-pin"></i>Montreal, Canada</p>
        <p class=" address"><i class="icon-phone"></i>(514)-692-9467</p>
        <p class=" address"><i class="icon-mail"></i><a href="mailto:Edua@info.com">contact@knowledgetime.org</a></p>
        <img src="images/footer-map.png" alt="we are here" class="img-responsive">
      </div>
    </div>
  </div>
</footer>
<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <p>Copyright &copy; 2016 <a href="home.php">KnowledgeTime</a>. all rights reserved.</p>
      </div>
    </div>
  </div>
</div>
<!--FOOTER ends-->

<script src="js/jquery-2.2.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootsnav.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery-countTo.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution.extension.video.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/functions.js"></script>

</body>
</html>
